Feature: SignIn screen.
  As a user, I want to be able to log in to the system.

  @debug
  Scenario: Leave blank Password field
    Given I go  to the  link https://showslinger-staging.herokuapp.com/users/sign_in	When I  leave  blank  Password  field.
	When I input valid value into Email field.
	When I  click  on  Login  button
	Then I should see an error message <result>.
    
  Scenario: Input wrong-format email 
    Given I go  to the  link https://showslinger-staging.herokuapp.com/users/sign_in	When I  input wrong-format email
	When I input valid value into Password field.
	When I  click on Continue button
	Then I should see an error message <result>.
    
  Scenario: Input non registered email address
    Given I go  to the  link https://showslinger-staging.herokuapp.com/users/sign_in	When I  input non registered Email address.
	When I input valid value into Password field.
	When I  click  on Continue button
	Then  I should see an error message <result>.
    
  Scenario: Input incorrect Email and Password
    Given I go  to the  link https://showslinger-staging.herokuapp.com/users/sign_in	When I input incorrect Email.
	When I input incorrect Password.
	Then I should see an error message <result>.
    
  Scenario: Input correct Email and Password
    Given I go  to the  link https://showslinger-staging.herokuapp.com/users/sign_in	When I input correct Email.
	When I input correct Password.
	Then navigate to Dashboard screen.
 
    
    