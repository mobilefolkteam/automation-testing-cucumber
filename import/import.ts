import {readFile, utils} from 'ts-xlsx';
import fs from 'fs';
import slugify from 'slugify';

const workbook = readFile(__dirname + '/tc.xlsx');

const genFeature = (_name: string, _description: string, _scenarios: any) => {
    let scenarios = ''
    // @ts-ignore
    for (let scenario: any of _scenarios) {
        if (scenario) {
            let procedures = (scenario["Procedures"].split('\n') || []).map((item: any) => (`${item.trim().replace(/\d\.\s*/, '\tWhen ')}\n`))
            scenarios += `
  Scenario: ${scenario["TC's_Title"]}
    Given ${scenario["Pre_conditions"]}\r${procedures.join('')}\tThen ${scenario['Expected_result']}
    `
        }
    }
    return `Feature: ${_name}\n  ${_description}

  @debug${scenarios}
    `;
}


const genSteps = (_feature: string, _scenarios: any) => {
    let scenarios: string = ''
    // @ts-ignore
    for (let scenario: any of _scenarios) {
        if (scenario) {
            let procedures = (scenario["Procedures"].split('\n') || []).map((item: any) => (`${item.replace(/\d\.\s*When/, '\tWhen')}\n`))
            // @ts-ignore
            scenarios += `Given(/^${scenario["Pre_conditions"].replace(/\//gm, "\\/").trim()}$/, async function (this: CustomWorld) {expect(1).toBe(1);});\n\n`
            for (let procedure of procedures) {
                scenarios += `When(/^${procedure.replace(/\d\.\s*/, '').replace('""', "'").trim()}$/, async function (this: CustomWorld) {expect(1).toBe(1);});\n\n`
            }
            scenarios += `Then(/^${scenario['Expected_result'].trim()}$/, async function (this: CustomWorld) {expect(1).toBe(1);});\n\n`
        }
    }
    return `import {CustomWorld} from '../../world';
import {Given, Then, When} from '@cucumber/cucumber';
import expect from 'expect';

${scenarios}
    `;
}

const first_sheet_name = workbook.SheetNames[0];
const worksheet = workbook.Sheets[first_sheet_name];
const data = utils.sheet_to_json(worksheet);
let _feature: any = data.splice(0, 1);
let _data = data.splice(1, data.length - 1);
let __feature: any = _feature[0]['#'].split('\n');
let nameFeature = __feature[0];
let desFeature = __feature[1];


console.log("https://showslinger-staging.herokuapp.com/users/sign_in".replace(/\//gm, "\\/"))

fs.writeFile(`features/${slugify(nameFeature.replace('.', '').toLowerCase())}.feature`, genFeature(nameFeature, desFeature, _data), function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("Data written successfully!");
    console.log("Let's read newly written data");
});
fs.writeFile(`step-definitions/${slugify(nameFeature.replace('.', '').toLowerCase())}-steps.ts`, genSteps(_feature[0]['#'], _data), function (err) {
    if (err) {
        return console.error(err);
    }
    console.log("Data written successfully!");
    console.log("Let's read newly written data");
});
