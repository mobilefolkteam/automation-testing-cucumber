import {CustomWorld} from '../world';
import {Given, Then, When} from '@cucumber/cucumber';
import expect from 'expect';

Given(/^I go  to the  link https:\/\/showslinger-staging.herokuapp.com\/users\/sign_in$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  leave  blank  Password  field.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input valid value into Email field.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  click  on  Login  button$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Then(/^I should see an error message <result>.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Given(/^I go  to the  link https:\/\/showslinger-staging.herokuapp.com\/users\/sign_in$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  input wrong-format email$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input valid value into Password field.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  click on Continue button$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Then(/^I should see an error message <result>.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Given(/^I go  to the  link https:\/\/showslinger-staging.herokuapp.com\/users\/sign_in$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  input non registered Email address.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input valid value into Password field.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I  click  on Continue button$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Then(/^I should see an error message <result>.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Given(/^I go  to the  link https:\/\/showslinger-staging.herokuapp.com\/users\/sign_in$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input incorrect Email.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input incorrect Password.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Then(/^I should see an error message <result>.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Given(/^I go  to the  link https:\/\/showslinger-staging.herokuapp.com\/users\/sign_in$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input correct Email.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

When(/^I input correct Password.$/, async function (this: CustomWorld) {expect(1).toBe(1);});

Then(/^navigate to Dashboard screen.$/, async function (this: CustomWorld) {expect(1).toBe(1);});


    