import {CustomWorld} from '../../world';
import {Given, Then, When} from '@cucumber/cucumber';
import expect from 'expect';

Given('I have a simple maths calculator', async function (this: CustomWorld) {
    expect(1).toBe(1)
});

Given('a variable is set to {int}', async function (this: CustomWorld) {
    expect(1).toBe(1)
});

When('I increment this variable by {int}', async function (this: CustomWorld) {
    expect(1).toBe(1)
});

Then('the variable should contain {int}', async function (this: CustomWorld) {
    expect(1).toBe(1);
});
